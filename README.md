fnyzer (Flexible Nets analYZER)
===============================

Installation
------------

```
pip install fnyzer
```

Documentation
-------------

https://fnyzer.readthedocs.io
