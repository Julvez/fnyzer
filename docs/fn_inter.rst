FN with intermediate states
---------------------------

In a FN with intermediate states, the variables of the different periods and
macroperiods can be accessed through the prefixes q and Q respectively, where a
macroperiod is a sequence of consecutive periods with same net structure. The
prefix qi\_ refers to variables of the period i and the prefix Qj\_ refers to
variables of the macroperiod j. The prefixes q and Q are set in LFlexN.qpre and
LFlexN.Qpre respectively.

The following fields can be used to specify a FN with intermediate states:

.. class:: 'solver': String denoting the solver to use

    See the 'solver' field in `FN specification`_.
    
.. _FN specification: fn_spec.html

.. class:: 'mapers': Links for markings and actions between consecutive periods

    Example:
   
    .. code:: python
    
        'mapers': [{'net': net1, 'thetas': 4*[0.05]},
                   {'outinm': from1to2m, 'outina': from1to2a, 'net': net2, 'thetas': 3*[.1]}],
    
    Each component of the list is associated to a macroperiod. In the example,
    'net': net1 specifies the net structure for the first macroperiod (net1 is a
    dictionary specifying a FN), and 'thetas': 4*[0.05]
    specifies that the macroperiod has 4 periods of length 0.05. In the second
    macroperiod the net structure is given by net2 and there are 3 periods of
    length 0.1.
    
    The link between final markings(actions) to initial
    markings(actions) of macroperiods is given by 'outinm' and 'outina' which
    specify the required relationships as list of expressions, e.g.:
    
    .. code:: python
    
        from1to2m = ["3*m['p1'] == m0['PrX']",
                     "m['p2']-1 <= m0['p2']", "m0['p2'] <= m['p2']+1",
                     "m0['p3']== 9"]
        from1to2a = ["at['t1']==a0['t1']", "at['t2']>=a0['t2']", "at['t2']<=a0['t2']"]


.. class:: 'extracons': List of extra constraints that must be satisfied

    These constraints will be included in the programming problem to be optimized.
   
    .. code:: python
        
        'extracons': ["q0_m0['p1']<= 2", "Q1_avm['p3']>= 10"].
    
    .. note::
    
        The extra constraints of each particular net are ignored, i.e.
        only extra constraints in this list will be considered.

.. class:: 'obj': Dictionary with the objective function and its sense

    Example:
   
    .. code:: python
        
        'obj': {'f': "sum((Q1_m[p]-2)*(Q1_m[p]-2) for p in ['PrX','p2'])", 'sense': 'min'}

.. class:: 'options': Dictionary that specifies options

    The keys of this dictionary are
    the same as the ones of the 'options' dictionary for FNs without intermediate
    states plus the following:

        'plotres': Boolean.
            If True plot the variables specified in 'plotvars'.

        'plotvars': Dictionary specifying the variables to plot
            Each key of plotvars is
            a variable name or 'evm' or 'eva'. Variable names are plotted as
            steps (stair-like plot). 'evm' and 'eva' plot the marking and
            actions evolution, i.e. lines connecting initial and final
            markings(actions) are plotted.
            
            Example:

            .. code:: python
    
                'plotvars': {'l0':['t1','t2'], 'evm':['p1','p2']}
            

    .. note::
    
        The 'options' of each particular net are ignored, i.e. only the
        options in this dictionary are considered.
