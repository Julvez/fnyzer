FN with MPC
-----------

FNs with Model Predictive Control (MPC) are specified by dictionaries similar
to the ones discussed above. The MPC parameters are set in a dictionary 'mpc'
located the 'options' field and the variables of the different intervals (or
periods) of the control horizon can be accessed through the prefixes qi\_ and Q0\_.

.. note::

    That the net structure of a FN with MPC is not allowed to change over time.
    Hence, there is only one macroperiod Q0\_.

Options
'''''''

As in a FN with intermediate states, the 'options' dictionary can contain the
fields 'plotres' and 'plotvars' which have the same meaning. Particular
fields of the 'options' dictionary are:

.. class:: 'antype': This field must be set to 'mpc'

.. class:: 'mpc': Dictionary specifying the MPC parameters

   The keys of this dictionary are:

    'firstinlen': Length of the first interval (or period) of MPC
        This value is also the minimum time length allowed for flexible intervals.

    'numsteps': Number of optimization steps
        This value is equivalent to the number of FNs to optimize. It holds
        that the final time of the optimization is equal to 'numsteps'*'firstinlen'.

    'maxnumins': maximum number of intervals (including the first interval)
        Horizon in terms of number of intervals. It must to be greater than or equal to 1.

    'flexins': Boolean
        True if you want intervals to stretch until they cover up to the
        final time. False implies that the length of the intervals is equal
        to 'firstinlen'.

Objective function
''''''''''''''''''

The objective function is defined as a list of objective functions. The
*ith* component of such a list (which in Python has index *i-1*) is used when
the current step being optimized has *i* intervals. The
objective function for several intervals can refer to the variables of different
periods and to the overall macroperiod (there is only one macroperiod as the
net structure is not allowed to change) through the prefixes qi\_ and Q0\_.
For instance, let us define:

.. code:: python

    obj1in = {'f': "m['A']", 'sense':'min'}  # Objective for one interval
    obj2in = {'f': "q0_m['A']+q1_m['B']+Q0_m['C']", 'sense':'min'} # Objective for two intervals
    obj = [obj1in, obj2in] # List of objective functions

Then, the objective function can be set as:

.. code:: python

    'obj': obj

If 'maxnumins' is equal to 1, the objective function does not need to be a list.

Extra constraints
'''''''''''''''''

Both time independent and time dependent constraints can be included as fields
in the dictionary that specifies the FN. Similarly to the objective function,
these constrainst are specified as a list. The *ith* component of the list
is a list of constraints that is considered when the current step being optimized
has *i* steps.

Time independent constraints can be included as:

.. code:: python

    'extracons': [
        ["alphar['R1']==0", "avm['p7']>=1.0"],  # List of constraints when the step has one interval
        ["q0_alphar['R1']==0", "q1_m['p2']<=6.0"] # List of constraints when the step has two intervals
        ]

Notice that  if the control horizon is 1, i.e. there is always one interval then
'extracons' is still a list of lists, e.g.

.. code:: python

    'extracons': [["alphar['R1']==0"]]

Time dependent constraints can be included as:

.. code:: python

    textracons: [
            [
             {'cond': "0.05<=time and time<=0.5", 'cons': "l0['F']==0"},
             {'cond': "0.51<=time", 'cons': "l0['F']==1"}
             ], # List of constraints when the step has one interval
            [
             {'cond': "time<=0.5", 'cons': "q0_l0['F']+q1_l0['F']==0"},
             {'cond': "time>=0.51", 'cons': "q0_l0['F']==1"},
             {'cond': "time>=0.51", 'cons': "q1_l0['F']==2"}
            ], # List of constraints when the step has two intervals
        ]

The constraint 'cons' is considered when the condition in 'cond' is True
where *time* is the starting time of the step.

Resets
''''''

Both the initial marking and the initial actions of a step can be reset if a
given condition over the state of the previous interval is satisfied. The
conditions and the reset constraints are specified in a list of dictionaries.
Example:

.. code:: python

    resets = [
        {'cond': "m['A']>=10 or avl['T']>=41",
         'm0cons': ["m0['A'] == 0.5*m['A']", "m0['B'] == 0.4"],
         'a0cons': ["a0['R'] == 2*at['R']", "a0['F'] == 5.0"]},
        {'cond': "0.2<=time and 100<=m['A']",
         'ntimes': 1,
         'm0cons': ["m0['A'] == m['A']+5", "m0['B'] == 0.5*m['B']"]}
        ]

where time is the starting time of the step. The variables that can be used in
the conditions are: *time*, *m*, *avm*, *at*, *l* and *avl* which respectively
refer to the time, final marking, average marking, number of remaining actions,
intensity and average intensity of the previous interval.

'm0cons' and 'a0cons' are used to reset initial markings and actions
respectively. 'm0cons' can only include the variables *m* (final marking of
previous interval) and *m0* (initial marking of current step). Similarly,
'a0cons' can only include *at* and *a0*. If 'ntimes' is included in the reset
dictionary, the reset will take place at most 'ntimes'.


