fnyzer Documentation 1.3.10
===========================

*fnyzer* (Flexible Nets analYZER) is an open-source Python package for the
analysis of Flexible Nets (FNs), a modeling formalism for dynamic systems.
*fnyzer* makes use of `pyomo`_ and an optimization solver to build and solve
optimization problems associated with the FNs. Once the problems are solved,
the obtained results can be plotted and saved in a spreadsheet.

This documentation first explains how *fnyzer* can be installed and executed,
and then describes how FNs can be specified and analysed.

**Table of contents**

.. _pyomo: http://www.pyomo.org/

.. toctree::
   :maxdepth: 2

   installation.rst
   getting_started.rst
   fn_spec.rst
   fn_inter.rst
   fn_mpc.rst
   aux_funcs.rst

..  Indices and tables
    ==================
    
    * :ref:`genindex`
    * :ref:`modindex`
    * :ref:`search`
