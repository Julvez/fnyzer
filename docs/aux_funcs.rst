Auxiliary functions
-------------------

In order to facilitate the specification of FNs, *fnyzer* provides:

- A function to convert `Cobra`_ models into FN dictionaries.
- A class to approximate nonlinear dynamics by piecewise linear functions.
  
.. _Cobra: https://opencobra.github.io/

Cobra models
~~~~~~~~~~~~

The function *cobra2fn()* converts a *Cobra* model into a FN specification.
It is defined as:

.. code:: python

    def cobra2fn(comodel, knockouts = [], solver = 'cplex'):

where *comodel* is a *Cobra* model, *knockouts* is a list of names of genes to be
knocked out before generating the FN, and *solver* is the name of the solver
to be used in future analysis.

The following lines assume that *Cobra* is installed in your system and that
the file *MODELXXX.xml* is a model in `SBML`_ format.

.. _SBML: http://sbml.org

.. code:: python

    from fnyzer import cobra2fn
    import cobra
    model = cobra.io.read_sbml_model('MODELXXX.xml')
    fndic = cobra2fn(model)

*fndic* is now a dictionary containing the FN specification that can be edited
and analyzed, see `Getting started`_.

.. _Getting started: getting_started.html


Nonlinear dynamics
~~~~~~~~~~~~~~~~~~

The class *regsheap*, see file `regsheap.py`_, can be used to create and store in
a priority queue regions that represent a piecewise linear approximation of a
nonlinear function. Such regions comply with the format required for FNs.

.. _regsheap.py: https://bitbucket.org/Julvez/fnyzer/raw/master/fnyzer/regsheap.py

The parameters required are the following (see below for an example):

.. class:: 'proregs': Dictionary with the initial borders of the regions

.. class:: 'func': Nonlinear function to be approximated

    The dynamics of the regions are given by 'func' which is approximated
    by a linear function. The function 'func' takes a matrix as an argument, the
    first column of the matrix is a constant vector 1 and the rest of the columns
    of the matrix are the paramenters of the function sorted alphabetically.
       
    The linear dynamics of each regions is obtained by linear regression
    (ordinary least squares). Once the regions are created, the region with
    maximum msr (mean squared residual) is split. A warning is reported if the
    predicted dynamics of a generated region can get negative values (such a
    region should be further split to get better precision and avoid negative values).

.. class:: 'parnames': Dictionary with names of net elements and parameters

    Dictionary that associates names of the elements connected with the
    intensity handler with the names of the parameters used in the linear expression:
    

        parnames['lap'] refers to the name of the variable to which the linear
        regression is assigned, usually an intensity sarc.
        
        parnames['place'] refers to the name of the parameter associated with
        the intensity edge ('place', 'shandler').
              
.. class:: 'maxregs', 'maxmsr': Maximum number of regions and mean squared residual

    The regions are further split until 'maxregs' regions are created or
    the msr (mean squared residual) of every region is lower than 'maxmsr'.

.. class:: 'sampoints': Number of points used to sample the function

    Number of points to be used in each dimension for the linear
    approximation (regression).

.. class:: 'pref': The names of the regions are prefixed with 'pref'


The use of *regsheap* is explemplified by the FN *hillpwlnet* in the file 
`hillpwlnet.py`_. The net models a reaction with a rate that follows
a particular Hill equation. In order to analyze the FN, download the file
in your working directory and execute the following line in a shell to
get steady state information:

.. _hillpwlnet.py: https://bitbucket.org/Julvez/fnyzer/raw/master/nets/hillpwlnet.py

::

    $ fnyzer hillpwlnet hillpwlnet

Execute the following to get a trajectory of the FN with MPC:

::

    $ fnyzer hillpwlnet hillpwlnetmpc


.. note::

    - The function to be approximated is assumed to depend just on the marking *m*.
    - The resulting approximation is assigned to just one intensity arc.
