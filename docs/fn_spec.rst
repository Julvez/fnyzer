FN specification
----------------

Flexible Nets (FNs) are specified by a Python dictionary with the
fields described in this section. Notice that such dictionary can be modified by
*fnyzer* to set default options and to adjust some parameters.

.. note::

    See Supplementary File S1 of https://doi.org/10.1038/s41540-017-0044-x for names and meaning of the FN parameters.

.. note::

    See keys in dictionaries *vdimdom* (files `FlexN.py`_ and `GFlexN.py`_) for names of the variables that are available in *fnyzer*. Those names can be used to define the objective function, to specify the variables to plot, to define extra constraints, etc.

.. _FlexN.py: https://bitbucket.org/Julvez/fnyzer/src/master/fnyzer/FlexN.py
.. _GFlexN.py: https://bitbucket.org/Julvez/fnyzer/src/master/fnyzer/GFlexN.py

The following fields can be used to specify a FN:

.. class:: 'name': String denoting the name of the net

    This string will be used to generate names for
    'xlsfile' and 'netfile' (see options below) if such
    fields are omitted.

.. class:: 'solver': String denoting the solver to use

    *fnyzer* has been mainly tested with 'gurobi', 'cplex' and 'glpk'.
    
    .. warning::
    
        GLPK does not support quadratic constraints and will not be able
        to solve some optimization problems.

.. class:: 'theta': Number denoting the length of the time interval for optimization

    This field is compulsory for transient analysis.

.. class:: 'places': Dictionary with names of places and initial markings

    A None value means that the initial marking is not specified, and hence,
    it is taken as a variable that can be constrained by 'm0cons' or 'extracons', e.g.

    .. code:: python
        
        'places': {'p1': 5, 'p2': None}
    
    
    A dictionary can also be associated with each place to describe it (only
    the value of 'm0' will be used during analysis, e.g.
    
    .. code:: python
        
        'p1': {'m0': 5, 'full_name': 'ATP', 'formula': 'cdcdsH'}

.. class:: 'trans': Dictionary with names of transitions, intensities and number of actions

    This dictionary contains the names of transitions, their default intensities
    and their initial number of actions. A None value means that the default intensity/initial
    actions is not specified, and hence, it is taken as a variable that can be
    constrained by 'l0cons'/'a0cons' or 'extracons', e.g.

    .. code:: python
        
        'trans': {'t1': {'l0': 2, 'a0': 1}, 't2': {'l0': None, 'a0': 0}}

.. class:: 'regs': Dictionary with names and specification of regions

    Each region is specified by a list of linear constraints, e.g.

    .. code:: python
        
        'regs': {'upr': ["50 >= m['p2']", "m['p2'] <= 100"],
                 'lor': ["m['p2'] <= 50"]}

.. class:: 'parts': Dictionary with names of partitions and associated regions

    Example:
   
    .. code:: python
        
        'parts': {'Part1': ['upr', 'lor'], 'Part2': ['regalpha', 'regbeta']}

.. class:: 'vhandlers': Dictionary with names and specification of event handlers

    Each event handler is specified by a list whose first component is a dictionary
    that links nicknames to connected arcs and edges, and the rest of components
    are the linear relations between the variables associated to the arcs and
    edges. 
    Example:

    .. code:: python
        
        'vhandlers': {'v1':[{'a': ('p1','v1'), 'b':('v1','p2'), 'c':('t1','v1')},
                            'a == b', 'a == c'],
                      'v2':[{'a':('p2','v2'), 'b':('v2','p1'), 'c':('t2','v2')},
                            'a <= b', 'b <= a', 'c == a']}
                            
    In general, any linear expression satisfying Python syntax, eg.
    "-2.0*a <= -b + c" can be used.
    
    .. warning::
    
       Nicknames have to follow the regular expression [_A-Za-z][_a-zA-Z0-9]* and
       cannot be Python keywords.

.. class:: 'shandlers': Dictionary with names and specification of intensity handlers

    The specification is similar to that of the event handlers plus the
    optional addition of guards for the linear relations in guarded nets.
    For instance, an intensity handler *s1* with unguarded and guarded arcs
    can be defined as:

    .. code:: python
        
        'shandlers': {'s1':[{'a':('p1','s1'), 'x':('s1','t1'), 'y':('s1','t2'), 'z':('s1','t3')},
                           'x==2*a',                              # unguarded arc
                           {'upr': ['2.1*a <= y', 'y <= 2.2*a'],
                            'lor': ['1.1*a <= y', 'y <= 1.2*a']}, # guarded arc
                           'x == 1+z']}                           # unguarded arc
    
    .. note::
    
       All places, transitions, vhandlers and shandlers must have different names.

.. class:: 'm0cons': List of initial marking constraints

    Example:
   
    .. code:: python
        
        'm0cons': ["100 <= m0['p1']", "m0['p1'] + 2*m0['p3']<= 110"]
    
    **Warning**: there must be no spaces or separators within the variables and
    brackets of the constraints, i.e. "m0 ['A'] == 0.5*m['A']" or
    "m0[ 'A'] == 0.5*m['A']" will raise an error.

.. class:: 'l0cons': List of default intensity constraints

    Example:
   
    .. code:: python
        
        'l0cons': ["l0['t1'] <= 8", "l0['t2'] >= 3"]

.. class:: 'a0cons': List of initial actions constraints

    Example:
   
    .. code:: python
        
        'a0cons': ["a0['t1']>=2.3", "a0['t2']<=12"]

.. class:: 'mcons':  List of final marking constraints

    Example:
   
    .. code:: python
        
        'mcons': ["m['p1']>= 15", "-19+m['p1']<=m['p2']"]

.. class:: 'mbounds': List of marking bounds

    These bounds are forced both for the final and average marking.
    Example:
   
    .. code:: python
        
        'mbounds': ["m['p1']>= 7", "9<=m['p2']"]

.. class:: 'actfplaces': List of places whose tokens must be active at the final state

    Instead of a list, it can be the string 'all' that accounts for all places.
    Example:
   
    .. code:: python
        
        'actfplaces': ['p1', 'p2']

.. class:: 'actavplaces': List of places whose tokens must be active all the time

    Instead of a list it can be the string 'all' that accounts for all places.
    Example:
   
    .. code:: python
        
        'actavplaces': ['p1', 'p2']

.. class:: 'exftrans': List of transitions with forced executions at the final state

    Instead of a list it can be the string 'all' that accounts for all
    transitions.
    Example:
   
    .. code:: python
        
        'exftrans': ['t1', 't2']

.. class:: 'exavtrans': List of transitions with forced executions over all the time interval

    Instead of a list it can be the string 'all' that accounts for all
    transitions.
   
    .. code:: python
        
        'exavtrans': ['t1', 't2']

.. class:: 'Ec' and 'Fc': Matrices Ec and Fc for equalities of intensitis at arcs

    Each component of Ec is a dictionary with the weights of the arcs.
    Example:
   
    .. code:: python
        
        'Ec':[{('s1','t1'): 2, ('s2','t2'): -1}, {('s3','t5'): 1}]
        'Fc':[0, 1]
    
    implies that the following constraints must hold:

    .. math:: 2\cdot\Delta\lambda[(s1,t1)] = \Delta\lambda[(s2,t2)]

    and

    .. math:: \Delta\lambda[(s3,t5)] = 1

.. class:: 'E' and 'F': Matrices E and F for inequalities of intensitis at arcs

    If a component of F is set to float('-inf') then a tight upper bound for the
    corresponding component of E will be computed.
    Example:
   
    .. code:: python
        
        'E': [{('s1','t1'): 1, ('s2','t2'): 1}, {('s3','t5'): 1}]
        'F': [10, float('-inf')]
    
    implies that:

    .. math:: \Delta\lambda[(s1,t1)] + \Delta\lambda[(s2,t2)] \leq 10

    must hold, and a upper bound *b* will be computed such that:
    
    .. math:: \Delta\lambda[(s3,t5)] \leq b

    will hold.

.. class:: 'wl', 'wu': Lower and upper bounds to linearize products of variables

    These values are used to linearize products of variables involving
    intensities and are computed automatically if they are not specified.

.. class:: 'W': Upper bound to linearize implications in guarded nets

    This value is computed automatically if it is not specified.

.. class:: 'extracons': List of extra constraints that must be satisfied

    These constraints will be included in the programming problem to be optimized.
    Example:
   
    .. code:: python
        
        'extracons': ["avm['PrX' ]>= 40", "m['p1']+avl['t1' ]>= 3"]

.. class:: 'obj': Dictionary with the objective function and its sense

    The key 'f' is the objective function, and the key 'sense' is the string 'min' or 'max'.
    Example:
   
    .. code:: python
        
        'obj': {'f': "m['P1']", 'sense': 'max'}
    
.. class:: 'options': Dictionary that specifies the following options

.. note::

    See dictionary *defoptions* in `FlexN.py`_ for default values of the options.

.. _FlexN.py: https://bitbucket.org/Julvez/fnyzer/src/master/fnyzer/FlexN.py
    
     'antype': String specifying the analysis type
        Possible values: 'tr' = transient, 'un' = untimed, 'st' = steady state, 'cst' = constant steady state, 'mpc' = model predictive control.
          
     'epsiloncompF': epsilon used in the exit condition to compute F
        \ 

     'epsilonLU': epsilon to check if L[k] and U[k] are too close
        If U[k]<=L[k]+epsilonLU, then: 1) k is removed from L, U, E, F and moved
        to Ec[k]; and 2) Fc[k] is set to the original U[k].

     'maxitcompF': Maximum number of iterations used in the exit condition to computet F
        \

     'allsatoE': Boolean
        If True all the intensity arcs will be included in E and the corresponding components of F will be set to float('-inf').
        If True the quadratic constraints are likely to be tighter and the bounds more precise.
        If True more CPU time is required, specially if the net is big or has many intermediate states.

     'printres': Boolean
        If True print values of variables and objective function.

     'writexls': Boolean
        If True write values of variables and objective function to the spreadsheet 'xlsfile'.

     'writevars': Variables to be written to the spreadsheet
        If 'all' all variables are written, otherwise they have to be specifies as a
        dictionary like: {'avmue':'all', 'l0':['t1','t2'], 'm':['p1','p2']}
        where 'all' stands for all the components of the key variable

     'writeLUnegsa': Boolean
        If False the LU components with one element and
        negative weight are not written.

     'xlsfile': Name of the spreadsheet where the results will be written
        \

     'savenet': Boolean
        If True save net after optimization, i.e., with all
        the computed variables, as a Python object to file 'netfile'. The net
        is saved with the method cPickle.dump().

     'netfile': Name of the file where the net object will be saved
        \

     'printmodel': Boolean
        If True print the programming problem (variables,
        constraints, objective function).

     'dsspe': Dictionary that specifies intervals for linearization.
        Dictionary {'type': , 'q': } that specifies how the intervals
        ds=[0,d1,..,dq-1,dq] to linearize the products of variables are
        generated.

        - 'type' can be either 'exp' for exponential, 'uni' for uniform, 'rand' for uniform plus random or 'shift' for slightly shifted uniform.

        - 'q' is the number of intervals (in 'rand' and 'shift' an optional key 'shf' can be used to define how much the values dj in ds are shifted).

     'epsilonalga': epsilon used to set alphar and gammar according to avdeltar.
        'epsilonalga' must be lower than 1/k where k is the number of regions of
        the partition with the highest number of regions
        (see (67), (96) and (97) of Supplementary file S1 of https://doi.org/10.1038/s41540-017-0044-x).
        
        .. warning:: 

            Avoid a very low 'epsilonalga' as it can cause numerical issues in
            the solver, e.g. the solver might violate a constraint by close to
            tolerance values.

     'scalebs': Numerical value to scale bounds wl, wu and W
        The bounds wl, wu and W are multiplied by 'scalebs' so that they
        are not tight.

