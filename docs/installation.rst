Installation and resources
--------------------------

The easiest way to install *fnyzer* is to use pip (the standard tool
for installing Python packages).
Execute the following line in a shell:

::

    $ pip install fnyzer

Requirements
~~~~~~~~~~~~

In order to solve the optimization problems associated with the flexible nets,
*fnyzer* requires a solver supported by `pyomo`_. For untimed
and steady state analysis of FNs, the `GLPK`_ solver can be used.
GLPK can be straightforwardly installed as a Debian package, e.g. in Ubuntu:

::

    $ apt-get install glpk-utils
    
Windows users can follow the instructions in this `this link`_ to install GLPK. In general, the set of constraints associated with the FNs include quadratic
constraints, and hence, *fnyzer* requires a solver that can handle such
constraints. *fnyzer* has been mainly tested with `Gurobi`_ and `CPLEX`_ (both
solvers offer free academic licenses).

.. _GLPK: https://www.gnu.org/software/glpk/
.. _this link: http://winglpk.sourceforge.net/
.. _pyomo: http://www.pyomo.org
.. _Gurobi: https://www.gurobi.com
.. _CPLEX: https://www.ibm.com/analytics/cplex-optimizer
    
    .. warning::
    
        GLPK does not support quadratic constraints and will not be able
        to solve some optimization problems.

The installation of at least one solver in your system is necessary to run
*fnyzer*. You can check that a solver is properly installed by executing these
commands:

::

    $ cplex
    Welcome to IBM(R) ILOG(R) CPLEX(R) Interactive Optimizer 12.6.1.0
    ...
    $ gurobi.sh
    Gurobi Interactive Shell (linux64), Version 6.0.0
    ...



Testing your installation
~~~~~~~~~~~~~~~~~~~~~~~~~

Download the file `fnexamples.py`_ in your working directory.
If you have CPLEX in your system, execute the following line:

.. _fnexamples.py: https://bitbucket.org/Julvez/fnyzer/raw/master/nets/fnexamples.py

::

    $ fnyzer fnexamples net0cplex

If you have Gurobi in your system, execute the following line:

::

    $ fnyzer fnexamples net0gurobi

If you have GLPK in your system, execute the following line:

::

    $ fnyzer fnexamples net0glpk

After the execution, the files *net0.xls* and *net0.pkl* should be in
your working directory.

Resources
~~~~~~~~~

*fnyzer* is hosted at Bitbucket:

* https://bitbucket.org/Julvez/fnyzer

Queries
~~~~~~~

Queries can be sent to fnyzer@unizar.es


Bibliography
~~~~~~~~~~~~

*Flexible Nets: A modeling formalism for dynamic systems with uncertain parameters*; 
J. Júlvez, S. G. Oliver; 
Discrete Event Dynamic Systems: Theory and Applications, 2019. https://doi.org/10.1007/s10626-019-00287-9

*Modeling, analyzing and controlling hybrid systems by Guarded Flexible Nets*; 
J. Júlvez, S. G. Oliver; 
Nonlinear Analysis: Hybrid Systems, 2019. https://doi.org/10.1016/j.nahs.2018.11.004

*Steady State Analysis of Flexible Nets*; 
J. Júlvez, S. G. Oliver; 
IEEE Transactions on Automatic Control, 2019. https://doi.org/10.1109/TAC.2019.2931836

*Handling variability and incompleteness of biological data by flexible nets: a case study for Wilson disease*; 
J. Júlvez, D. Dikicioglu, S. G. Oliver; 
npj Systems Biology and Applications, 2018. https://doi.org/10.1038/s41540-017-0044-x 
