"""
Examples of flexible nets
Use:
    fnyzer fnexamples stonet
"""
# Remark: Differente solvers (glpk, gurobi and cplex) are used in the examples
# Use the one you like best or the one you have installed
# (glpk can only be used for untimed analysis as it does not support
# quadratic constraints)

################### Stoichiometry of reaction R: 2A + B -> C

# Use: fnyzer fnexamples stonet
# Output: stonet.xls, stonet.pkl
stonet = { 
    'name': 'stonet',
    'solver': 'glpk',
    'places': {
        'A': {'m0': 6}, 
        'B': {'m0': 4},
        'C': {'m0': 0},
    }, 
    'vhandlers': {
        'v': [
            {'a': ('A','v'), 'b': ('B','v'), 'c': ('v','C')}, 
             'a == 2*b', 'b == c'
            ],
    },
    'obj': {'f': "m['C']", 'sense': 'max'},
    'options' : {
        'antype': 'un'
    }
}

################### Stoichiometry of reaction R: 2A + B -> C with uncertain
################### initial concentrations

# Use: fnyzer fnexamples unstonet
# Output: unstonet.xls, unstonet.pkl
unstonet = {
    'name': 'unstonet',
    'solver': 'glpk',
    'places': {
        'A': {'m0': None},
        'B': {'m0': 4},
        'C': {'m0': 0},
    },
    'm0cons': ["4 <= m0['A']", "m0['A'] <= 7"],
    'vhandlers': {
        'v': [
            {'a': ('A','v'), 'b': ('B','v'), 'c': ('v','C')},
             'a == 2*b', 'b == c'
            ],
    },
    'obj': {'f': "m['C']", 'sense': 'max'},
    'options' : {
        'antype': 'un'
    }
}

################### FN with exchange reactions

# Use: fnyzer fnexamples excnet
# Output: excnet.xls, excnet.pkl
excnet = {
    'name': 'excnet',
    'solver': 'glpk',
    'places': {
        'A': {'m0': 3},
        'B': {'m0': 2},
        'C': {'m0': 5},
    },
    'trans': {
        't1': {'l0': None, 'a0': 0},
        't3': {'l0': 0, 'a0': 0},
        't4': {'l0': None, 'a0': 0},
    },
    'vhandlers': {
        'v1': [
            {'a': ('v1','A'), 'v': ('t1','v1')},
             'a == v'
        ],
        'v2': [
            {'b': ('v2','B')}
        ],
        'v3': [
            {'a': ('A','v3'), 'b': ('B','v3'), 'c': ('v3','C'), 'v': ('t3','v3')},
             'a == v', 'b == v', 'c == v'
        ],
        'v4': [
            {'c': ('C','v4'), 'v': ('t4','v4')},
             'c == v'
        ],
    },
    'shandlers': {
        's3': [
            {'x': ('s3','t3'), 'a': ('A','s3')},
            'x == a',
            ],
    },
    'l0cons': ["1 <= l0['t1']", "l0['t1'] <= 4", "l0['t4'] <= 3"],
    'actfplaces': ['A'], # A is connected to s3 and must be active
    'actavplaces': ['A'],
    'exftrans': 'all',   # All transitions are forced to fire
    'exavtrans': 'all',
    'obj': {'f': "avm['A']", 'sense': 'max'},
    'options' : {
        'antype': 'st'
    }
}

################### Guarded FN

# Use: fnyzer fnexamples guardnet
# Output: guardnet.xls, guardnet.pkl
guardnet = { 
    'name': 'guardnet',
    'solver': 'cplex',
    'places': {'A': 10, 'B': 0},
    'trans': {'t1': {'l0': 0.0, 'a0': 0}, 't2': {'l0': 0, 'a0': 0}},
    'vhandlers': {
        'v1': [{'a': ('A','v1'), 'v': ('t1','v1')}, 'a == v'],
        'v2': [{'b': ('v2','B'), 'v': ('t2','v2')}, 'b == v'],
    },
    'regs': {
        'off': ["m['A'] >= 5"],
        'on':  ["m['A'] <= 5"],
    },
    'parts': {'Par': ['off', 'on']},
    'shandlers': {
        's1': [{'a': ('A','s1'), 'x': ('s1','t1'), 'y': ('s1','t2')},
                'x == a',
                {'off': ['y == 0.2'], 'on': ['y == a']}]
    },
    'actfplaces': ['A'],
    'exftrans': 'all',
    'actavplaces': ['A'],
    'exavtrans': 'all',
    'obj': {'f': "m['B']", 'sense':'min'},
    'options': {
        'antype' : 'mpc',
        'mpc': {
            'firstinlen': .1,
            'numsteps':   30,
            'maxnumins':  1,
            'flexins': False
        },
        'writevars': {'m': ['A', 'B'], 'l': 'all', 'L': 'all', 'U': 'all'},
        'plotres': True,
        'plotvars': {'evm':['A', 'B']}
    }
}


################### Untimed cycle of two places

net0 = {
    'name': 'net0',
    'solver': 'glpk',  # Solver to be used
    'places': {        # Places and initial marking
        'p1': 5,       # Initial marking of p1
        'p2': None,    # The initial marking of p2 is not specified
        }, 
    'm0cons': ["1 <= m0['p2']", "m0['p2'] <= 2"], # Constraints for the
                                                  # initial marking of p2
    'vhandlers': {     # Event handlers
        'v1': [
            {'a': ('p1','v1'), 'b': ('v1','p2')}, # The same amount of tokens
             'a == b'                             # is consumed from p1 and
            ],                                    # produced in p2
        'v2': [
            {'a': ('p2','v2'), 'b': ('v2','p1')}, 
             'a == b'
            ],
        },
    'obj': {'f': "m['p1']", 'sense': 'max'}, # Objective function
    'options' : {
        'antype': 'un',        # Untimed analysis
        'xlsfile': 'net0.xls', # Results will be stored in this spreadsheet
        'netfile': 'net0.pkl', # The flexible net object with the resulting
                               # variables will be saved in this file
        }
    }

net0glpk = net0.copy()
net0glpk['solver'] = 'glpk'
net0gurobi = net0.copy()
net0gurobi['solver'] = 'gurobi'
net0cplex = net0.copy()
net0cplex['solver'] = 'cplex'


################### Timed cycle of two places and two transitions

net1 = {
    'name': 'net1',
    'solver': 'cplex',
    'theta': 0.1, # Time instant at which the net will be optimized
    'places': {
        'p1': {'m0': 5, 'full_name': 'First place', 'formula': 'H2O'}, 
        'p2': {'m0': 1, 'full_name': 'Second place', 'description': 'Tank of water'},
        }, 
    'trans': {
        't1': {'l0': 0, 'a0': 0}, 
        't2': {'l0': 0, 'a0': 0},
        }, 
    'vhandlers': {
        'v1': [
            {'a': ('p1','v1'), 'b': ('v1','p2'), 'c': ('t1','v1')}, 
            'a == b', 
            'a == c',
            ],
        'v2': [
            {'a': ('p2','v2'), 'b': ('v2','p1'), 'c': ('t2','v2')}, 
            'a == b', 
            'a == c',
            ],
        },
    'shandlers': { # Intensity handlers. The intensity of the transitions is
                   # proportional to the marking of places
        's1': [
            {'x': ('p1','s1'), 'z': ('s1','t1')},
            'z == x',
            ], 
        's2': [
            {'x': ('p2','s2'), 'z': ('s2','t2')},
            'z == x',
            ],
        },
    'actfplaces': 'all', # The tokens of all the places are forced to be active
    'actavplaces': 'all',
    'exftrans': 'all',   # The actions of all the transitions are forced to be executed
    'exavtrans': 'all',
    'obj': {'f': "2*m['p1']-m['p2']", 'sense': 'min'},
    'options' : {
        'antype': 'tr',        # Transient analysis
        'netfile': 'net1.pkl', # If you want to store the results in a different
                               # folder, use 'netfile': 'results/net1.pkl'
        'xlsfile': 'net1.xls',
        }
    }

net1i = { # net1 with intermediate states
    'name': 'net1i',
    'solver': 'gurobi',
    'mappers': [{'net': net1, 'thetas': 10*[0.1]}], # The state is computed at 10 time
                                                   # instants at intervals of 0.1
    'obj': {'f': "Q0_m0['p1']", 'sense': 'min'},   # Dummy objective function
    'options' : {
        'plotres' : True,                  # A plot will be produced
        'plotvars' : {'evm':['p1','p2']},  # Variables to be included in the plot
        },
    }


#################### Exponential decay with uncertain rate (model predictive control)

expdmpc = { 
    'name': 'expdmpc',
    'solver': 'cplex',
    'places': {'A': 5},
    'trans': {'R': {'l0': 0, 'a0': 0}},
    'vhandlers': {'v': [{'a': ('A','v'), 'r': ('R','v')}, 'a == r']},
    'shandlers': {'s': [{'a': ('A','s'), 'r': ('s','R')}, '0.9*a <= r', 'r <= 1.1*a']},
    'actfplaces': 'all',
    'exftrans': 'all',
    'actavplaces': 'all',
    'exavtrans': 'all',
    'obj': {'f': "m['A']", 'sense':'min'}, # A different trajectory is obtained if 
                                           # the sense is changed
    'options': {
        'antype' : 'mpc',
        'mpc': {
            'firstinlen': .1,
            'numsteps':   40,
            'maxnumins':  1,
            'flexins': False
            },
        'writevars': {'at': ['R'], 'avae': [('R','v')], 'm': ['A'],
                      'l': 'all', 'L': 'all'},
        'plotres': True,
        'plotvars': {'evm':['A']}
        }
}

################### Oscillator

oscil = { # Net structure for oscili
    'places': {'p1': 9, 'p2': 12},
    'trans' : {
        't1': {'l0': 0, 'a0': 0},
        't2': {'l0': 12, 'a0': 0},
        't3': {'l0': 10, 'a0': 0},
        't4': {'l0': 0, 'a0': 0},
        },
    'vhandlers': {
        'v1': [{'a': ('t1','v1'), 'b': ('v1','p1')}, 'a == b'],
        'v2': [{'a': ('t2','v2'), 'b': ('p1','v2')}, 'a == b'],
        'v3': [{'a': ('t3','v3'), 'b': ('v3','p2')}, 'a == b'],
        'v4': [{'a': ('t4','v4'), 'b': ('p2','v4')}, 'a == b'],
        },
    'shandlers': {
        's1': [{'x': ('p2','s1'), 'z': ('s1','t1')}, 'z == x'],
        's2': [{'x': ('p1','s2'), 'z': ('s2','t4')}, 'z == x'],
        },
    'actfplaces': 'all',
    'exftrans': 'all',
    'actavplaces': 'all',
    'exavtrans': 'all',
    }
    
oscili = { # Oscillator with intermediate steps
    'name': 'oscili',
    'solver': 'cplex',
    'mappers': [{'net': oscil, 'thetas': 20*[0.2]}],
    'obj': {'f': "q0_m0['p1']", 'sense': 'max'},
    'options' : {
        'antype': 'tr',
        'printres': True,
        'printmodel': False,
        'plotres': True,
        'plotvars': {'evm':['p1','p2']}
        }
}

oscilmpc = { # Oscillator with mpc
    'name': 'oscilmpc',
    'solver': 'cplex',
    'places': {'p1': 9, 'p2': 12},
    'trans' : {
        't1': {'l0': 0, 'a0': 0},
        't2': {'l0': 12, 'a0': 0},
        't3': {'l0': 10, 'a0': 0},
        't4': {'l0': 0, 'a0': 0},
        },
    'vhandlers': {
        'v1': [{'a': ('t1','v1'), 'b': ('v1','p1')}, 'a == b'],
        'v2': [{'a': ('t2','v2'), 'b': ('p1','v2')}, 'a == b'],
        'v3': [{'a': ('t3','v3'), 'b': ('v3','p2')}, 'a == b'],
        'v4': [{'a': ('t4','v4'), 'b': ('p2','v4')}, 'a == b'],
        },
    'shandlers': {
        's1': [{'x': ('p2','s1'), 'z': ('s1','t1')}, 'z == x'],
        's2': [{'x': ('p1','s2'), 'z': ('s2','t4')}, 'z == x'],
        },
    'actfplaces': 'all',
    'exftrans': 'all',
    'actavplaces': 'all',
    'exavtrans': 'all',
    'obj': {'f': "m['p1']", 'sense': 'min'},
    'options': {
        'antype' : 'mpc',
        'mpc': {
            'firstinlen': .2,
            'numsteps': 40,
            'maxnumins': 1,
            'flexins': False,
            },
        'plotres': True,
        'plotvars': {'evm':['p1','p2']}
        },
    }
    
################### Guarded flexible net with two regions on/off

ractive = { # Repressor decay activates production
    'name': 'ractive',
    'solver': 'cplex',
    'places': {'A': 5, 'B': 0},
    'trans': {'R1': {'l0': 0, 'a0': 0}, 'R2': {'l0': 0.1, 'a0': 0}},
    'vhandlers': {
        'v1': [{'a': ('A','v1'), 'r': ('R1','v1')}, 'a == r'],
        'v2': [{'b': ('v2','B'), 'r': ('R2','v2')}, 'b == r']
        },
    'regs': {
        'off': ["m['A'] >= 1"],
        'on':  ["m['A'] <= 1"],
        },
    'parts': {'Par': ['off', 'on']},                      
    'shandlers': {
        's1': [{'a': ('A','s1'), 'r': ('s1','R1')}, 'a == r'],
        's2': [{'r': ('s2','R2')}, {'off': ['r == 0'], 'on': ['r == 1']}]
        },
    'actfplaces': ['A'],
    'exftrans': 'all',
    'actavplaces': ['A'],
    'exavtrans': 'all',
    'obj': {'f': "m['B']", 'sense':'min'},
    'options': {
        'antype' : 'mpc',
        'mpc': {
            'firstinlen': .1,
            'numsteps':   40,
            'maxnumins':  1,
            'flexins': False
            },
        'writevars': {'m': ['A', 'B'], 'l': 'all', 'L': 'all', 'U': 'all'},
        'plotres': True,
        'plotvars': {'evm':['A', 'B']}
        }
}

################### Oscillator with switching dynamics (guarded flexible net)
# More details of this net can be found in section 4 of:
# Modeling, analyzing and controlling hybrid systems by Guarded Flexible Nets
# J. Julvez, S. G. Oliver
# Nonlinear Analysis: Hybrid Systems, 2019. https://doi.org/10.1016/j.nahs.2018.11.004

horizgos = 1 # Horizon, in number of steps, for the guarded oscillator
goscilmpc = { # Oscillator with regions
    'name': 'goscilmpc',
    'solver': 'cplex',
    'places': {'p1': 4, 'p2': 12},
    'trans' : {
        't1': {'l0': 0, 'a0': 0},
        't2': {'l0': 12, 'a0': 0},
        't3': {'l0': 10, 'a0': 0},
        't4': {'l0': 0, 'a0': 0},
        },
    'vhandlers': {
        'v1': [{'a': ('t1','v1'), 'b': ('v1','p1')}, 'a == b'],
        'v2': [{'a': ('t2','v2'), 'b': ('p1','v2')}, 'a == b'],
        'v3': [{'a': ('t3','v3'), 'b': ('v3','p2')}, 'a == b'],
        'v4': [{'a': ('t4','v4'), 'b': ('p2','v4')}, 'a == b'],
        },
    'regs' : {
        'lor': ["m['p2'] <= 8"],
        'mir': ["8 <= m['p2']", "m['p2'] <= 15"],
        'upr': ["15 <= m['p2']"],
        },
    'parts': {'ThePar': ['lor', 'mir', 'upr']},
    'shandlers': {
        's1': [
            {'x': ('p2','s1'), 'z': ('s1','t1')}, 
             {
             'lor': ['z == 1.5*x'],
             'mir': ['z == x'],
             'upr': ['z == 1.5*x'],
             }],
        's2': [{'x': ('p1','s2'), 'z': ('s2','t4')}, 'z == x'],
        },
    'actfplaces': 'all',
    'exftrans': 'all',
    'actavplaces': 'all',
    'exavtrans': 'all',
    'obj': {'f': "m['p1']", 'sense': 'min'},
    'options': {
        'dsspe': {'type': 'uni', 'q': 1}, # As we force the model to be
                                          # in one region during each step,
                                          # we don't need intervals here
        'antype' : 'mpc',
        'mpc': {
            'firstinlen': .1,
            'numsteps': 170,
            'maxnumins': horizgos,
            'flexins': False,
            },
        'plotres': True,
        'plotvars': {'evm':['p1','p2']}
        },
    }

# Force the model to be in one region during each step to speed up computation
extracons = [["alphar['lor'] + alphar['mir'] + alphar['upr'] == 1"]]
for i in range(1,horizgos):
    auxcon = []
    for j in range(i+1): 
        auxcon.append("q"+str(j)+"_alphar['lor'] + q"+str(j)+"_alphar['mir'] == 1 + q"+str(j)+"_alphar['upr'] == 1")
    extracons.append(auxcon)
goscilmpc['extracons'] = extracons



################### Resource allocation problem
# More details of this net can be found in Section 5.2 of:
# Flexible Nets: A modeling formalism for dynamic systems with uncertain parameters
# J. Julvez, S. G. Oliver
# Discrete Event Dynamic Systems: Theory and Applications, 2019. https://doi.org/10.1007/s10626-019-00287-9

sraloc = { # Auxiliary net for sraloci
    'places': {'pa': 2, 'pb': 1, 'p1': 3, 'p2': 0, 'p3': 2, 'p4': 0, 'p5': 0},
    'trans' : {'t1': {'l0': 0, 'a0': 0}, 't2': {'l0': 0, 'a0': 0}, 't3': {'l0': 0, 'a0': 0}},
    'vhandlers': {
        'v1': [{'a': ('p1','v1'), 'b': ('v1','p2'), 'c': ('t1','v1')}, 
               'a == b', 'a == c'],
        'v2': [{'a': ('p3','v2'), 'b': ('v2','p4'), 'c': ('t2','v2')},
               'a == b', 'a == c'],
        'v3': [{'a': ('v3','p5'), 'b': ('t3','v3')},
               'a == b']
        },
    'shandlers': {
        's1': [{'x': ('pa','s1'), 'z': ('s1','t1')}, 'x == z'], 
        's2': [{'x': ('pa','s2'), 'y': ('pb','s2'), 'z': ('s2','t2')}, 
               'x == y', 'x == z'],
        's3': [{'x': ('pb','s3'), 'z': ('s3','t3')}, 'x == z'],                
        },
    'exftrans': 'all',
    'exavtrans': 'all',
}

x = 10
nsteps = 9*x
lstep = 0.5/x
sraloci = { # Resource allocation with intermediate states
    'name': 'sraloci',
    'solver': 'cplex',
    'mappers': [{'net': sraloc, 'thetas': nsteps*[lstep]}],
    'obj': {'f': "Q0_avm['p2'] + 0.5*Q0_avm['p4'] + 0.25*Q0_avm['p5']", 'sense': 'max'},
    'options': {
        'antype': 'tr',
        'allsatoE': False,
        'plotres': True,
        'plotvars': {
            'evm': ['p2','p4','p5'],
            'avmue': [('pb','s3')],
            }
        }
}


################### Cycle of three places and one control action
# More details of this net can be found in Section 5.3 of:
# Flexible Nets: A modeling formalism for dynamic systems with uncertain parameters
# J. Julvez, S. G. Oliver
# Discrete Event Dynamic Systems: Theory and Applications, 2019. https://doi.org/10.1007/s10626-019-00287-9

lincon = { 
    'name': 'lincon',
    'solver': 'gurobi',
    'places': {'p1': 0, 'p2': 0, 'p3': 9},
    'trans' : {
        't1': {'l0': 0, 'a0': 0},
        't2': {'l0': 0, 'a0': 0},
        't3': {'l0': 0, 'a0': 0},
        't4': {'l0': None, 'a0': 0},
        },
    'l0cons': ["l0['t4'] <= 1.5"],
    'vhandlers': {
        'v1': [{'a': ('t1','v1'), 'b': ('p1','v1'), 'c': ('v1','p2')}, 'a == b', 'a == c'],
        'v2': [{'a': ('t2','v2'), 'b': ('p2','v2'), 'c': ('v2','p3')}, 'a == b', 'a == c'],
        'v3': [{'a': ('t3','v3'), 'b': ('p3','v3'), 'c': ('v3','p1')}, 'a == b', 'a == c'],                
        },
    'shandlers': {
        's1': [{'x': ('p1','s1'), 'r': ('s1','t1')}, 'r == x'],
        's2': [{'x': ('p2','s2'), 'r': ('s2','t2')}, 'r == x'],
        's3': [{'x': ('p3','s3'), 'r': ('s3','t3')}, 'r == x'],                
        's4': [{'x': ('t4','s4'), 'y': ('s4','t1'), 'z': ('t2','s4')}, 'y == x', 'z == 2.0*x'],                
        },
    'actfplaces': 'all',
    'exftrans': 'all',
    'actavplaces': 'all',
    'exavtrans': 'all',
    'obj': {'f': "(m['p1']-1)*(m['p1']-1)+(m['p2']-4)*(m['p2']-4)", 'sense': 'min'},
    'options': {
        'antype' : 'mpc',
        'mpc': {
            'firstinlen': .1,
            'numsteps': 40,
            'maxnumins': 1,
            'flexins': False,
            },
        'plotres': True,
        'plotvars': {'evm':['p1','p2', 'p3'], 'avl':['t1','t2'], 'l0': ['t4']}
        },
    }

linconi = { # lincon with intermediate steps
    'name': 'linconi',
    'solver': 'gurobi',
    'mappers': [{'net': lincon, 'thetas': 45*[0.1]}],
    'obj': {'f': "(Q0_avm['p1']-1)*(Q0_avm['p1']-1)+(Q0_avm['p2']-4)*(Q0_avm['p2']-4)", 'sense': 'min'},
    'options' : {
        'antype': 'tr',
        'printres': False,
        'printmodel': False,
        'plotres': True,
        'plotvars': {'evm':['p1','p2', 'p3'], 'avl':['t1','t2'], 'l0': ['t4']}
        }
}

