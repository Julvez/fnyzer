"""
Flexible net with piecewise linear approximations of the intensities.
The flexible net models the reaction A -> 0 with rate [A]^3/(1+[A]^3)
(particular Hill equation)
Dictionaries generated:
    hillpwlnet:   flexible net
    hillpwlnetmpc:flexible net under model predictive control
Use:
    fnyzer hillpwlnet hillpwlnet
    fnyzer hillpwlnet hillpwlnetmpc
"""
from copy import deepcopy
from fnyzer import regsheap

# MPC parameteres
firstinlen = 0.30
numsteps = 20
maxnumins = 1

# Parameters for the regions to be generated
low_border, upp_border = 0.2, 3.6  # Lower and upper borders
maxmsr = 1e-5       # Maximum value allowed for the mean squared residual of the regions
maxregs = 6         # Maximum number of regions to be generated
m0 = upp_border-0.1 # Initial marking

# Reaction rate of A -> 0
def hill(X):
    return X[:,1]**3/(1 + X[:,1]**3)

# This preliminary region 'freg' avoids linearization near 0 and
# hence negative predicted values of the linear regression/approximation
borderfs = { # Borders of region freg
    'freg': ["m['A'] <= " + str(low_border)],
    } 
shdynfs = { # Dynamics in region 'freg' (set to avoid discontinuities in the reaction rate,
            # and be proportional to [A])
    'freg': ['r == a*'+str(low_border**2/(1+low_border**3))],
    }

# Generation of regions with approximated dynamics
Rproregs = {'A': [low_border, upp_border]}  # Initial borders of the regions to be generated
Rregsheap = regsheap(
    Rproregs, hill, {'lap': 'r', 'A': 'a'}, maxmsr = maxmsr, maxregs = maxregs
    )

# FN dictionary
hillpwlnet = {
    'name': 'hillpwlnet',
    'solver': 'cplex',
    'theta': .1,
    'places': {'A': m0},
    'trans' : {'R': {'l0': 0, 'a0': 0}},
    'vhandlers': {'v': [{'a': ('A','v'), 'c': ('R','v')}, 'a == c']},
    'regs': dict(Rregsheap.regborders(), **borderfs),
    'parts': {'Part': Rregsheap.regnames() + [k for k in borderfs.keys()]},
    'shandlers': {
        's': [
            {'a':('A','s'), 'r':('s','R')}, dict(Rregsheap.shdyns(), **shdynfs)
            ]
        },
    'actfplaces': 'all',
    'exftrans': 'all',
    'actavplaces': 'all',
    'exavtrans': 'all',
    'obj': {'f': "avm['A']", 'sense': 'min'},
    'options': {
        'antype': 'st',
        'dsspe': {'type': 'uni', 'q': 12}
        }
}

# FN with model predictive control
hillpwlnetmpc = deepcopy(hillpwlnet) 
hillpwlnetmpc['name'] = 'hillpwlnetmpc'
hillpwlnetmpc['obj'] = {'f': "m['A']", 'sense': 'min'}
hillpwlnetmpc['options'] = {
    'antype': 'mpc',
    'mpc': {
        'firstinlen': firstinlen,
        'numsteps':   numsteps,
        'maxnumins':  maxnumins,
        'flexins': False
        },
    'plotres': True,
    'plotvars': {'evm':['A'], 'avl':['R']}
    }
